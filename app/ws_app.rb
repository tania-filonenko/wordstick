module WS
  class App
    def initialize
    end

    def self.instance
      @instance ||= Rack::Builder.new { run WS::App.new }.to_app
    end

    def call(env)
      response = WS::API.call(env)
    end
  end
end
