require 'grape-swagger'
module WS
  class API < Grape::API
    prefix 'api'
    format :json
    mount ::WS::Counter
    mount ::WS::Statistics
    add_swagger_documentation
  end
end
