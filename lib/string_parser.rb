module WS
  class StringParser < BaseParser
    def self.satisfied?(source)
      source.is_a? String
    end

    def perform
      process_line(source)
    end

    private

    def type
      'String'
    end
  end
end
