module WS
  class BaseParser
    attr_reader :source, :db_instance

    def initialize(source)
      @source = source
      @db_instance = WS::Storage.new
    end

    def perform
      raise NotImplementedError
    end

    private

    def satisfied?
      raise NotImplementedError
    end

    def type
      raise NotImplementedError
    end

    def process_line(line)
      words = line.split(/\s*[.,"\s]\s*/)
      words.each do |word|
        word.downcase!
        save_to_db(word)
      end
    end

    def save_to_db(word)
      old_value = db_instance.read(word)
      new_value = old_value.to_i + 1
      db_instance.write(word, new_value)
    end
  end
end
