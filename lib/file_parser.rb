require 'base_parser'

module WS
  class FileParser < BaseParser
    def self.satisfied?(source)
      File.file?(source)
    end

    def perform
      File.open(source, 'r') do |file|
        file.each_line do |line|
          process_line(line)
        end
      end
    end

    private

    def type
      'File'
    end
  end
end
