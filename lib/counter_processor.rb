module WS
  class CounterProcessor
    attr_reader :source

    def initialize(source)
      @source = source
    end

    def perform
      strategies = [
        FileParser,
        HtmlParser,
        StringParser
      ]
      strategy = strategies.detect { |s| s.satisfied?(source) }
      return unless strategy.present?

      strategy.new(source)&.perform
    end
  end
end
