module WS
  class StatisticsProcessor
    attr_reader :word, :db_instance

    def initialize(word)
      @word = word
      @db_instance = WS::Storage.new
    end

    def perform
      word.delete!('\\"')
      db_instance.read(word)
    end
  end
end
