require 'base_parser'
require 'open-uri'

module WS
  class HtmlParser < BaseParser
    def self.satisfied?(source)
      uri = URI.parse(source)
      %w( http https ).include?(uri.scheme)
    rescue URI::BadURIError
      false
    rescue URI::InvalidURIError
      false
    end

    def perform
      open(source) do |file|
        file.each_line do |line|
          process_line(line)
        end
      end
    end

    private

    def type
      'HTML'
    end
  end
end
