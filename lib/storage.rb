module WS
  class Storage
    attr_reader :instance

    def initialize
      @instance = Redis.new(host: 'redis')
    end

    def read(key)
      instance.get(key) || '0'
    end

    def write(key, value)
      instance.set key, value.to_s
    end
  end
end
