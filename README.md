# Grape Web API on Rack running in Docker container 

* Run app in docker using composer:
```
$ docker-compose up
```

* Run tests
```
docker-compose run web rspec
```

* Display all available routes:
```
$ rake routes
```

* Example for manual testing

File:
```
http://localhost:9292/api/count?source=files/example.txt
```
HTML:
```
http://localhost:9292/api/count?source=https://www.dccomics.com/characters/the-flash
```
String:
```
http://localhost:9292/api/count?source=just other words
```

* Documentation
```
http://localhost:9292/api/swagger_doc
```
