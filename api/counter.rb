module WS
  class Counter < Grape::API
    format :json
    
    params do
      requires :source, type: String
    end

    post '/count' do
      result = CounterProcessor.new(params[:source]).perform
      if result
        { status: :ok }
      else
        { status: :fail }
      end
    end
  end
end
