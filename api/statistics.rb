module WS
  class Statistics < Grape::API
    format :json
    
    params do
      requires :word, type: String
    end

    get '/statistics' do
      result = StatisticsProcessor.new(params[:word]).perform
      if result
        { occurrence: result }
      else
        { status: :fail }
      end
    end
  end
end
