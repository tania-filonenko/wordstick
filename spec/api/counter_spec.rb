require 'spec_helper'

describe WS::API do
  include Rack::Test::Methods

  def app
    WS::API
  end

  context "with a word that didn't occur yet" do
    subject { post '/api/count', { source: 'the' } }

    let(:redis_instance) { instance_double(Redis) }

    before do
      allow(Redis).to receive(:new).and_return(redis_instance)
      allow(redis_instance).to receive(:get).and_return(nil)
    end

    it "saves the word's occurrence into redis" do
      expect(redis_instance).to receive(:set).with('the', '1')
      subject
    end
  end

  context "with a word that occurred twice" do
    subject { post '/api/count', { source: 'my' } }

    let(:redis_instance) { instance_double(Redis) }
    let(:word) { 'my' }

    before do
      allow(Redis).to receive(:new).and_return(redis_instance)
      allow(redis_instance).to receive(:get).with(word).and_return('2')
    end

    it "saves the word's occurrence into redis" do
      expect(redis_instance).to receive(:set).with(word, '3')
      subject
    end
  end
end
