require 'spec_helper'

describe WS::API do
  include Rack::Test::Methods

  def app
    WS::API
  end

  context "with a word that didn't occur yet" do
    it 'returns 0 occurrence count' do
      get '/api/statistics', { word: 'the' }
      expect(last_response.status).to eq(200)
      expect(last_response.body).to eq({ occurrence: '0' }.to_json)
    end
  end

  context "with a word that didn't occur yet" do
    let(:redis_instance) { instance_double(Redis) }
    let(:word) { 'my' }
    let(:occurrence) { '3' }

    before do
      allow(Redis).to receive(:new).and_return(redis_instance)
      allow(redis_instance).to receive(:get).with(word).and_return(occurrence)
    end

    it 'returns 0 occurrence count' do
      get '/api/statistics', { word: word }
      expect(last_response.status).to eq(200)
      expect(last_response.body).to eq({ occurrence: occurrence }.to_json)
    end
  end
end
